import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-list-book-item',
  templateUrl: './list-book-item.component.html',
  styleUrls: ['./list-book-item.component.css']
})

export class ListBookItemComponent {

  @Input() public book;
  @Input() public i;
  constructor(
    private router: Router,
    private bookService: BooksService) { }

  editBook(i) {
   this.router.navigate(['/editBook', i]);
  }

  deleteBook(i) {
     this.bookService.deleteBook(i);
  }

  showDetails(i) {
    this.router.navigate(['/listBooks', i]) ;
  }

}

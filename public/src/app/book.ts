export class Book {

    constructor(
        public title: string,
        public author: string,
        public isbnNo: string,
        public desc: string) { }
}

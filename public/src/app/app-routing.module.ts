import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { AddBookComponent } from './add-book/add-book.component';
import { ListBooksComponent } from './list-books/list-books.component';
import { HomeComponent } from './home/home.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { BookDetailComponent } from './book-detail/book-detail.component';

const routes: Routes =
 [
    { path: '', redirectTo: 'home', pathMatch: 'full'} ,
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'addBook', component: AddBookComponent },
    { path: 'listBooks', component: ListBooksComponent },
    { path: 'editBook/:id', component: EditBookComponent },
    { path: 'listBooks/:id', component: BookDetailComponent },
    { path: '**', component: AddBookComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes)],
    exports: [ RouterModule]
})

export class AppRoutingModule {

}

export const routingComponents =
[
    AddBookComponent,
    ListBooksComponent,
    HomeComponent,
    EditBookComponent,
    BookDetailComponent
];

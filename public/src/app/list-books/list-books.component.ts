import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.css']
})

export class ListBooksComponent implements OnInit {

  constructor(
     private router: Router,
     private bookService: BooksService) { }

 public books = [];

  ngOnInit() {
    this.books = this.bookService.getBooks();
  }

  addMoreBooks() {
    this.router.navigate(['/addBook']);
  }

  editBook(i) {
   this.router.navigate(['/editBook', i]);
  }

  deleteBook(i) {
     this.bookService.deleteBook(i);
  }

  showDetails(i) {
    this.router.navigate(['/listBooks', i]);
  }

  gotoHome() {
    this.router.navigate(['/home']);
  }

}

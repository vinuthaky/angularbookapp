import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})

export class BookDetailComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private bookService: BooksService) { }

  public book: Book;
  public index: number;

  ngOnInit() {
      this.route.paramMap.subscribe((params: ParamMap) =>    {
      this.index = parseInt(params.get('id'), 10);
      console.log(this.index);
      this.book = this.bookService.getBook(this.index);
    });
  }

  editBook() {
   this.router.navigate(['/editBook', this.index]);
  }

  deleteBook() {
     this.bookService.deleteBook(this.index);
     this.router.navigate(['/listBooks']);
  }

  gotoHome() {
    this.router.navigate(['/home']);
  }

  goBack() {
    this.router.navigate(['/listBooks']);
  }

}
